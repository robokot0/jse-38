# JSE-38
## Software requirements
|Software|Version|
|----------------|---------|
|Java|openjdk version "11"|
|Apache Maven|Apache Maven 3.6.1|
|OS|Windows 8.1|
## Description of the technology stack
Apache Maven 3
## Contacts
|Name|e-mail|
|----------------|---------|
|Korshunov Andrey|robokot0@gmail.com|
## Commands for building the application
|Command|Result|
|----------------|---------|
|mvn clean|Clearing the project directory|
|mvn package|Creating executable files|
## GIT
<https://gitlab.com/robokot0/jse-38>  

