package jse38.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import jse38.emtity.Joke;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class JokeRepositoryTest {

    @Test
    void getInstance() {
        JokeRepository jokeRepository = JokeRepository.getInstance();
        assertEquals(jokeRepository,JokeRepository.getInstance());
    }

    @Test
    void findCreate() {
        assertDoesNotThrow(()->{
            JokeRepository jokeRepository = JokeRepository.getInstance();
            jokeRepository.create("find1121");
            Joke joke = jokeRepository.find("find1121").orElseThrow(Exception::new);
            assertEquals(joke.getCategory(),"find1121");
            Optional<Joke> jokenull = jokeRepository.find("find112111");
            assertFalse(jokenull.isPresent());
        });
    }

    @Test
    void findAllClear() {
        assertDoesNotThrow(()->{
            JokeRepository jokeRepository = JokeRepository.getInstance();
            jokeRepository.create("find1121");
            assertNotEquals(jokeRepository.findAll().size(),0);
            jokeRepository.clear();
            assertEquals(jokeRepository.findAll().size(),0);
        });
    }

    @Test
    void loadCategoriesFrom() {
        JokeRepository jokeRepository = JokeRepository.getInstance();
        assertDoesNotThrow(()->{
            jokeRepository.loadCategoriesFrom(new ObjectMapper(),new ByteArrayInputStream("[\"animal\",\"career\",\"celebrity\",\"dev\",\"explicit\",\"fashion\",\"food\",\"history\",\"money\",\"movie\",\"music\",\"political\",\"religion\",\"science\",\"sport\",\"travel\"]".getBytes()));
            List<Joke> jokeList = jokeRepository.findAll();
            assertEquals(jokeList.size(),16);
        });
    }

    @Test
    void loadJokeFrom() {
        JokeRepository jokeRepository = JokeRepository.getInstance();
        assertDoesNotThrow(()->{
            jokeRepository.loadJokeFrom(new ObjectMapper(),"animal",new ByteArrayInputStream("{\"categories\":[\"animal\"],\"created_at\":\"2020-01-05 13:42:19.104863\",\"icon_url\":\"https://assets.chucknorris.host/img/avatar/chuck-norris.png\",\"id\":\"30lywvzfsowzxkiiy2voyw\",\"updated_at\":\"2020-01-05 13:42:19.104863\",\"url\":\"https://api.chucknorris.io/jokes/30lywvzfsowzxkiiy2voyw\",\"value\":\"Chuck Norris does not own a house. He walks into random houses and people move.\"}".getBytes()));
            Joke joke = jokeRepository.find("animal").orElseThrow(Exception::new);
            assertNotNull(joke);
            assertEquals(joke.getCategory(),"animal");
            assertEquals(joke.getText(),"Chuck Norris does not own a house. He walks into random houses and people move.");
        });
    }
}