package jse38;

import jse38.controller.JokeController;

import java.io.IOException;
import java.net.URISyntaxException;

public class App {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        JokeController jokeController = JokeController.getInstance();
        jokeController.loadJokes();
        jokeController.viewJokes();
    }
}