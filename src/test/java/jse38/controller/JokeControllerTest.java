package jse38.controller;

import jse38.emtity.Joke;
import jse38.service.JokeService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class JokeControllerTest {
    static JokeController jokeController;
    static JokeService jokeService;

    @BeforeAll
    static void BeforeAll(){
        jokeService= Mockito.mock(JokeService.class);
        jokeController = new JokeController(jokeService);
    }

    @Test
    void getInstance() {
        JokeController jokeController = JokeController.getInstance();
        assertEquals(jokeController,JokeController.getInstance());
    }

    @Test
    void loadJokes() {
        assertDoesNotThrow(()->{
            jokeController.loadJokes();
            Mockito.verify(jokeService,Mockito.times(1)).loadJokes();
        });
    }

    @Test
    void viewJokes() {
        assertDoesNotThrow(()->{
            List<Joke> jokeList = new ArrayList<>();
            jokeList.add(new Joke("testviewJokes"));
            Mockito.doReturn(jokeList).when(jokeService).findAll();
            jokeController.viewJokes();
        });
    }
}