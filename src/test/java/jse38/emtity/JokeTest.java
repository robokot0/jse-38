package jse38.emtity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JokeTest {

    @Test
    void setTextGetText() {
        Joke joke = new Joke("category");
        joke.setText("text");
        assertEquals(joke.getText(),"text");
    }


    @Test
    void getCategory() {
        Joke joke = new Joke("category");
        assertEquals(joke.getCategory(),"category");
    }

    @Test
    void testToString() {
        Joke joke = new Joke("category");
        joke.setText("text");
        assertEquals(joke.toString(),"category:text");
    }
}