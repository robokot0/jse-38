package jse38.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import jse38.emtity.Joke;
import jse38.repository.JokeRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class JokeServiceTest {
    static JokeService jokeService;
    static JokeRepository jokeRepository;
    static HttpClient httpClient;
    static ObjectMapper objectMapper;
    static HttpResponse jokeHttpResponse;
    static String jokeListBdy = "[\"testcategory\"]";
    static List<Joke> jokeList = new ArrayList<>();
    @BeforeAll
    static void beforeAll() {
        jokeRepository = Mockito.mock(JokeRepository.class);
        httpClient = Mockito.mock(HttpClient.class);
        objectMapper = Mockito.mock(ObjectMapper.class);
        jokeService = new JokeService(jokeRepository,httpClient,objectMapper);
        jokeHttpResponse = Mockito.mock(HttpResponse.class);
        jokeList.add(new Joke("testjoke"));
    }

    @Test
    void getInstance() {
        JokeService jokeService = JokeService.getInstance();
        assertEquals(jokeService,JokeService.getInstance());
    }

    @Test
    void loadJokes() {
        assertDoesNotThrow(()->{
            Mockito.doReturn(jokeHttpResponse).when(httpClient).send(Mockito.any(),Mockito.any());
            Mockito.doReturn(jokeListBdy).when(jokeHttpResponse).body();
            Mockito.doReturn(jokeList).when(jokeRepository).findAll();
            jokeService.loadJokes();
        });
    }

    @Test
    void findAll() {
        assertDoesNotThrow(()->{
            jokeService.findAll();
            Mockito.verify(jokeRepository,Mockito.times(1)).findAll();
        });
    }
}