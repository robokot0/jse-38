package jse38.controller;


import jse38.service.JokeService;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Controller of Joke object
 */
public class JokeController {
    /**
     * private constructor for simple singleton
     */
    private JokeController() {
        jokeService = JokeService.getInstance();
    }
    /**
     * constructor for test
     * @param jokeService jokeService object
     */
    JokeController(JokeService jokeService) {
        this.jokeService = jokeService;
    }
    /**
     * instance for simple singleton
     */
    private static JokeController instance = null;

    /**
     * get instance of simple singleton
     * @return simple singleton
     */
    public static JokeController getInstance(){
        if (instance == null){
            instance = new JokeController();
        }
        return instance;
    }

    /**
     * JokeService object
     */
    final JokeService jokeService;
    /**
     * load joke category list from api.chucknorris.io
     * load random jokes from api.chucknorris.io for evry category
     * @throws IOException exception from HttpClient
     * @throws InterruptedException exception from HttpClient
     * @throws URISyntaxException exception from HttpClient
     */
    public void loadJokes() throws IOException, InterruptedException, URISyntaxException {
        jokeService.loadJokes();
    }
    /**
     * view all jokes
     */
    public void viewJokes(){
        jokeService.findAll().forEach((joke)-> System.out.println(joke.toString()));
    }
}
