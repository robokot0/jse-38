package jse38.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import jse38.emtity.Joke;
import jse38.repository.JokeRepository;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

/**
 * Service of Joke object
 */
public class JokeService {
    /**
     * private constructor for simple singleton
     */
    private JokeService() {
        jokeRepository = JokeRepository.getInstance();
        httpClient = HttpClient.newHttpClient();
        objectMapper = new ObjectMapper();
    }
    /**
     *  HttpClient object
     */
    final private HttpClient httpClient;
    /**
     * mapper of format of stream
     */
    final private ObjectMapper objectMapper;
    /**
     * instance for simple singleton
     */
    private static JokeService instance;
    /**
     * get instance of simple singleton
     * @return instance of simple singleton
     */
    public static JokeService getInstance(){
        if (instance == null){
            instance = new JokeService();
        }
        return instance;
    }
    /**
     * JokeRepository object
     */
    final JokeRepository jokeRepository;
    /**
     * constructor for test
     * @param jokeRepository JokeRepository object
     * @param httpClient HttpClient object
     * @param objectMapper ObjectMapper object
     */
    JokeService(JokeRepository jokeRepository, HttpClient httpClient, ObjectMapper objectMapper) {
        this.jokeRepository = jokeRepository;
        this.httpClient = httpClient;
        this.objectMapper = objectMapper;
    }
    /**
     * load joke category list from api.chucknorris.io
     * load random jokes from api.chucknorris.io for evry category
     * @throws IOException exception from HttpClient
     * @throws InterruptedException exception from HttpClient
     * @throws URISyntaxException exception from HttpClient
     */
    public void loadJokes() throws IOException, InterruptedException, URISyntaxException {
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(new URI("https://api.chucknorris.io/jokes/categories")).GET().build();
        HttpResponse<String> jokeList = httpClient.send(httpRequest,HttpResponse.BodyHandlers.ofString());
        jokeRepository.loadCategoriesFrom(objectMapper,new ByteArrayInputStream(jokeList.body().getBytes()));
        for (Joke joke:jokeRepository.findAll()) {
            HttpRequest jokeRequest = HttpRequest.newBuilder().uri(new URI("https://api.chucknorris.io/jokes/random?category=" + joke.getCategory())).GET().build();
            HttpResponse<String> jokeResponse = httpClient.send(jokeRequest,HttpResponse.BodyHandlers.ofString());
            jokeRepository.loadJokeFrom(objectMapper,joke.getCategory(),new ByteArrayInputStream(jokeResponse.body().getBytes()));
        }
    }
    /**
     * return list of all jokes from joke repository
     * @return list of all jokes
     */
    public List<Joke> findAll() {
        return jokeRepository.findAll();
    }
}
