package jse38.repository;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jse38.emtity.Joke;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Repository of Joke object
 */
public class JokeRepository {
    /**
     * private constructor for simple singleton
     */
    private JokeRepository() {
    }
    /**
     * static instance for simple singleton
     */
    private static JokeRepository instance = null;

    /**
     * get instance of simple singleton
     * @return instance
     */
    public static JokeRepository getInstance(){
        if (instance == null){
            instance = new JokeRepository();
        }
        return instance;
    }

    /**
     * Joke list with control of uniqueness of joke category in list
     */
    protected SortedMap<String, Joke> jokes = new TreeMap<>();
    /**
     * find joke by category
     * @param category category of joke
     * @return optional with joke
     */
    public Optional<Joke> find(String category) {
        return Optional.ofNullable(jokes.get(category));
    }
    /**
     * create joke and add it to repository
     * @param category category of joke
     * @return added to repository joke
     */
    public Joke create(String category) {
        return find(category).orElseGet(()-> {
            Joke joke = new Joke(category);
            jokes.put(category,joke);
            return joke;
        });
    }
    /**
     * return list with all jokes from repository
     * @return list with all jokes from repository
     */
    public List<Joke> findAll() {
        List<Joke> jokeList = new ArrayList<>();
        jokes.forEach((jokeCategory,joke)->jokeList.add(joke));
        return jokeList;
    }
    /**
     * remove all jokes from repository
     */
    public void clear(){
        jokes.clear();
    }

    /**
     * load categories of jokes from input stream
     * @param objectMapper mapper of format of stream
     * @param inputStream  input stream
     * @throws IOException exception from input stream
     */
    public void loadCategoriesFrom(ObjectMapper objectMapper, InputStream inputStream)  throws IOException {
        clear();
        JavaType type = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, String.class) ;
        List<String> jokeCategoryList = objectMapper.readValue(inputStream, type);
        jokeCategoryList.forEach(this::create);
    }

    /**
     * load joke from input stream
     * @param objectMapper mapper of format of stream
     * @param jokeCategory joke category
     * @param inputStream input stream
     * @throws IOException exception from input stream
     */
    public void loadJokeFrom(ObjectMapper objectMapper, String jokeCategory, InputStream inputStream) throws IOException {
        Joke joke = create(jokeCategory);
        JsonNode node = objectMapper.readValue(inputStream,JsonNode.class);
        joke.setText(node.path("value").asText());
    }


}
