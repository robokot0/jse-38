package jse38.emtity;

/**
 * joke text with joke category object
 */
public class Joke {
    /**
     * Joke text
     */
    private String text;
    /**
     * Joke category
     */
    private final String category;

    /**
     * constructor
     * @param category joke category
     */
    public Joke(String category) {
        this.category = category;
    }

    /**
     * getter for joke text
     * @return joke text
     */
    public String getText() {
        return text;
    }

    /**
     * setter for joke text
     * @param text joke text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * getter for joke category
     * @return joke category
     */
    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return (category==null?"не указана категория":category) + ":" + (text==null?"не указана шутка":text);
    }
}
